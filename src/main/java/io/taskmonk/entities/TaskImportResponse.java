package io.taskmonk.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The response object on importing tasks to a batch. Contains the job_id which should be used in a consequent
 * {@link io.taskmonk.client.TaskMonkClient#getJobProgress(String)} to monitor status of the upload
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskImportResponse {
    public String job_id;
    public String batch_id;
    // For backward compatibility
    public String batchId;

    @JsonProperty("batch_id")
    public String getBatchId() {
        return batch_id;
    }

    @JsonProperty("batch_id")
    public void setBatchId(String batch_id) {
        this.batch_id = batch_id;
        this.batchId = batch_id;
    }

    @JsonProperty("job_id")
    public String getJobId() {
        return job_id;
    }

    @JsonProperty("job_id")
    public void setJobId(String job_id) {
        this.job_id = job_id;
    }

    public TaskImportResponse() {

    }
    public TaskImportResponse(String job_id) {
        this.job_id = job_id;
    }
    public TaskImportResponse(String job_id, String batch_id) {
        this.job_id = job_id;
        this.batchId = batch_id;
        this.batch_id = batch_id;
    }

    @Override
    public String toString() {
        return "job_id = " + job_id + "; batchId = {}" + batchId + "; batch_id = " + batch_id;
    }
}
