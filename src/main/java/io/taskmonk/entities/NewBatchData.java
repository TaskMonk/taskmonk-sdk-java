
package io.taskmonk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class used for creating and editing batch properties
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewBatchData {
    /**
     * The name of the batch. Mandatory when creating a new batch.
     */
    String batch_name;

    /**
     * Optional priority for the batch. Higher priority batches are executed first.
     */
    Short priority;

    /**
     * Any instruction that is displayed to the analysts as they are working on tasks in the batch
     */
    String comments = "";

    /**
     * A start time in the future for when work should start on the batch
     */
    Date startTime = new Date();

    /**
     * A set of notifications for milestones on the batch
     */
    public List<Notification> notifications = new ArrayList<Notification>();

    public NewBatchData(String batch_name) {
        this.batch_name = batch_name;
    }

    public NewBatchData() {

    }

    @JsonProperty("batch_name")
    public String getBatchName() {
        return batch_name;
    }

    @JsonProperty("batch_name")
    public NewBatchData setBatchName(String batch_name) {
        this.batch_name = batch_name;
        return this;
    }

    @JsonProperty("start_time")
    public Date getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public NewBatchData setStartTime(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    public Short getPriority() {
        return priority;
    }

    public NewBatchData setPriority(Integer priority) {
        this.priority = priority.shortValue();
        return this;
    }

    public String getComments() {
        return comments;
    }

    public NewBatchData setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public NewBatchData setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
        return this;
    }
}

