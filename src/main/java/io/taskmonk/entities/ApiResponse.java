package io.taskmonk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Bean providing job response details
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse {
    public Boolean status;
    public String message;

    public ApiResponse() {

    }
    public ApiResponse(Boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public String toString() {
        return "status = " + status + "; message = " + message;
    }

}
