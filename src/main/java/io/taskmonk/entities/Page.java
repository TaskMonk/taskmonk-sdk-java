package io.taskmonk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Page<T> {
    /**
     * The page number for the paginated result. The first page number will be 1
     */
    Integer page;

    /**
     * The page size that was set in the query. Can be used to check if more items are present by matching
     * against items size
     */
    Integer pageSize;

    /**
     * The total number of items that matched against the query
     */
    Integer total;

    /**
     * The list of items returned for the query
     */
    List<T> items;

    @JsonProperty("page_number")
    public Integer getPageNumber() {
        return page;
    }

    @JsonProperty("page_number")
    public void setPageNumber(Integer pageNumber) {
        this.page = pageNumber;
    }

    @JsonProperty("page_size")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("page_size")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
