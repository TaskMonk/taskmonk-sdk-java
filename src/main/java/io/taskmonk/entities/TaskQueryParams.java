package io.taskmonk.entities;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class used to specify the tasks that need to be retrieved
 */
public class TaskQueryParams {

    /**
     * The list of fields to be retrieved. If this is not set, all fields will be returned
     */
    List<String> fields = new ArrayList<String>();

    /**
     * Retrieve tasks that were completed after this time. If this is not set, all tasks will be returned
     */
    Date taskCompleteStart;

    /**
     * Retrieve tasks that were completed before this time. If this is not set, all tasks will be returned
     */
    Date taskCompleteEnd;

    /**
     * Retrieve tasks that match the state. Allowed values are:
     * <p></p>
     * "COMPLETED"
     * <p></p>
     * "PENDING"
     * <p></p>
     * If not set, all tasks are returned
     */
    String taskState;

    /**
     * The number of tasks to be returned. If not set the default number of 500 tasks will be returned.
     */
    Integer pageSize;

    /**
     * The page number of the tasks to be returned. If not set, page number 1 will be returned. This should
     * be set to 1 more than the page number returned in the task query result
     */
    Integer pageNumber;

    String format = "dd-MM-yyyy";
    SimpleDateFormat df = new SimpleDateFormat(format);
    public List<String> getFields() {
        return fields;
    }

    public TaskQueryParams setFields(List<String> fields_names) {
        this.fields = fields_names;
        return this;
    }

    public Date getTaskCompleteStart() {
        return taskCompleteStart;
    }

    public TaskQueryParams setTaskCompleteStart(Date taskCompleteStart) {
        this.taskCompleteStart = taskCompleteStart;
        return this;
    }

    public Date getTaskCompleteEnd() {
        return taskCompleteEnd;
    }

    public TaskQueryParams setTaskCompleteEnd(Date taskCompleteEnd) {
        this.taskCompleteEnd = taskCompleteEnd;
        return this;
    }

    public String getTaskState() {
        return taskState;
    }

    /**
     * Retrieve tasks that match the state. Allowed values are:
     * <p></p>
     * "COMPLETED"
     * <p></p>
     * "PENDING"
     * <p></p>
     * If not set, all tasks are returned
     */
    public TaskQueryParams setTaskState(String taskState) {
        this.taskState = taskState;
        return this;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public TaskQueryParams setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public TaskQueryParams setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
        return this;
    }

    public List<NameValuePair> getParameters() {
        List<NameValuePair> result = new ArrayList<NameValuePair>();
        if (fields != null && fields.size() > 0) {
            for (String field :fields) {
                result.add(new BasicNameValuePair("field_names[]", field));

            }
        }
        if (pageSize != null) {
            result.add(new BasicNameValuePair("page_size", pageSize.toString()));
        }
        if (pageNumber != null) {
            result.add(new BasicNameValuePair("page_no", pageNumber.toString()));
        }
        if (taskCompleteStart != null) {
            result.add(new BasicNameValuePair("task_complete_start", df.format(taskCompleteStart)));
        }
        if (taskCompleteEnd != null) {
            result.add(new BasicNameValuePair("task_complete_end", df.format(taskCompleteEnd)));
        }
        if (taskState != null) {
            result.add(new BasicNameValuePair("task_state", taskState));
        }
        return result;
    }
}
