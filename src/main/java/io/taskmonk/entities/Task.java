package io.taskmonk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Holds the data for a single Task. This is used for both uploading data to Taskmonk and for
 * receiving updates from Taskmonk
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {
    static final Logger logger = LoggerFactory.getLogger(Task.class);
    /**
     * The batch id for the task. This is set only when retrieving tasks from multiple projects. This
     * need not be set when uploading tasks to Taskmonk
     */
    public String batchId;

    /**
     * The external id is used for correlation when sending and receiving tasks. The same externalId will be set
     * when Taskmonk sends an update for the task back. This is optional.
     */
    public String externalId;

    /**
     * The data for the task. The key would be the field name and the value would be the field value. When Taskmonk
     * sends back the updated task, this would contain the output field values also.
     */
    public Map<String, String> data;

    /**
     * This flag indicates that the data is encoded. Taskmonk will use this to determine whether the data needs to be
     * decoded.
     */
    public boolean isEncoded;

    public Task() {

    }
    public Task(String batchId, String externalId,
                Map<String, String> data, Boolean encode) {
        this.batchId = batchId;
        this.externalId = externalId;
        this.data = (encode) ? encodeData(data) : data;
        this.isEncoded = encode;
    }

    public Task(String batchId, String externalId,
                Map<String, String> data) {
        this(batchId, externalId, data, true);
    }

    public Task(String externalId,
                Map<String, String> data) {
        this.externalId = externalId;
        this.data = data;
    }

    @JsonProperty("batch_id")
    public String getBatchId() {
        return batchId;
    }

    @JsonProperty("batch_id")
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }


    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("external_id")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = encodeData(data);
    }

    @Override
    public String toString() {
        return "Task: " + batchId + " : " + externalId;
    }
    private Map<String, String> encodeData(Map<String, String> data) {
        Map<String, String> escapedData = new HashMap<String, String>();
        for (Map.Entry<String, String> entry: data.entrySet()) {
            try {
                escapedData.put(entry.getKey(), URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8.toString()));
            } catch (UnsupportedEncodingException e) {
                logger.error(entry.getValue(), e);
            }
        }
        return escapedData;
    }
}
