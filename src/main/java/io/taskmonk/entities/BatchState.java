package io.taskmonk.entities;

/**
 *
 * Returns a state for the batch. The supported state are:
 *<p></p>
 * "ACTIVE" - The batch is being worked on by the analysts
 *<p></p>
 * "PENDING" - Work has not started on the batch
 *<p></p>
 * "SCHEDULED" - A start time for the batch has been set and will be worked on after that date
 * <p></p>
 * "CANCELLED" - The execution of the batch has been cancelled by the customer
 * <p></p>
 * "INACTIVE" - The execution of the batch has been set to inactive
 * <p></p>
 * "DELETED" - The batch has been deleted and cannot be worked upon anymore
 * <p></p>
 * "ARCHIVED" - The batch has been archived and cannot be worked upon anymore
 * <p></p>
 * "COMPLETED" - Work on this batch has been completed
 */
public enum BatchState {
    PENDING,
    SCHEDULED,
    ACTIVE,
    CANCELLED,
    INACTIVE,
    DELETED,
    ARCHIVED,
    COMPLETED
}
