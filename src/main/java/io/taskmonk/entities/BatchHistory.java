package io.taskmonk.entities;


public class BatchHistory {
    /**
     * This is the id of the batch for which the history is to be fetched.
     */
    String batch_id;
    /**
     * The last saved state for the batch.
     */
    String state;
    /**
     * Time at which the state change was recorded.
     */
    Long last_updated;
    /**
     * The userId of the user if they performed any explicit update.
     */
    String user_id;
    /**
     * Any comment recorded at the time of updating the batch.
     */
    String comment;

    BatchHistory batchHistory;
    public BatchHistory(){

    }

    public BatchHistory(String batch_id, String state, Long last_updated, String user_id, String comment){
        this.batch_id = batch_id;
        this.state = state;
        this.last_updated = last_updated;
    }
}
