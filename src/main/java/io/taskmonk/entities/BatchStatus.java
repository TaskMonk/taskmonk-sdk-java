package io.taskmonk.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Status of a batch
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchStatus {
    /**
     * Number of tasks in progress.
     */
    Integer in_progress;
    /**
     * Number of tasks in the batch that have been completed
     */
    Integer completed;

    /**
     * Number of tasks in the batch that were rejected due to missing mandatory fields
     */
    Integer rejected;

    /**
     * The total number of valid tasks in the batch
     */
    Integer total;

    /**
     * Pending tasks for QC
     */
    Integer qc_pending;

    /**
     * {@link BatchState}
     */
    BatchState state;

    /**
     * Task Completion Percentage
     */
    Integer percentage_completed;

    public BatchStatus() {

    }
    public BatchStatus(Integer in_progress, Integer completed, Integer total, Integer qc_pending, Integer rejected, Integer percentage_completed) {
        this.completed = completed;
        this.total = total;
        this.in_progress = in_progress;
        this.qc_pending = qc_pending;
        this.percentage_completed = percentage_completed;
        this.rejected = rejected;
    }

    public Integer getCompleted() {
        return completed;
    }

    public void setCompleted(Integer completed) {
        this.completed = completed;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }


    public BatchState getState() {
        return state;
    }

    public void setState(BatchState state) {
        this.state = state;
    }

    public Integer getIn_progress() {return in_progress;}

    public Integer getQc_pending() {return qc_pending;}

    public Integer getRejected() {return rejected;}

    public Integer getPercentage_completed() {return percentage_completed;}

    public void setPercentage_completed(Integer percentage_completed) { this.percentage_completed = percentage_completed; }

    @Override
    public String toString() {
        return "Total = " + total + "; Completed = " + completed + "; In Progress = " + in_progress + "; state = " + state + "; Complete Percentage = " + percentage_completed + ";";
    }
}
