package io.taskmonk.streaming.impl;

import io.taskmonk.entities.Task;

import java.util.List;
import java.util.Map;

public class NewTasks {
    public String project_id;
    public String batch_id;
    public List<Task> tasks;
}
