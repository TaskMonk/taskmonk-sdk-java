package io.taskmonk.streaming.impl;

import io.taskmonk.streaming.azure.ServiceBusListener;


public class MessageStreamListener extends ServiceBusListener {

    public MessageStreamListener(String queueName, String accessKey) {
        super(queueName, accessKey);
    }
}
