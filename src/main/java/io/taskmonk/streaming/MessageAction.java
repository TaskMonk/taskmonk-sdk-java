package io.taskmonk.streaming;

/**
 * An indication of how the message should be handled in the queue
 * <p>
 * COMPLETE - The message processing is complete and can be removed from the queue
 *<p>
 * ABANDON - The message processing failed. The message remains in the queue and will be attempted again
 */
public enum MessageAction {
    COMPLETE,
    ABANDON
}
