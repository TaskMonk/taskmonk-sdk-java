package io.taskmonk.integrations.azure;

import com.microsoft.azure.servicebus.Message;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.ReceiveMode;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;


public class ServiceBusSendInterface {
    private static final Logger logger = LoggerFactory.getLogger(ServiceBusSendInterface.class);
    String queueName;
    String accessKey;
    String accessKeyName = "Client";
    String connectionString;

    public ServiceBusSendInterface(String queueName, String accessKey) {

        this.queueName = queueName;
        this.accessKey = accessKey;
        connectionString = String.format("Endpoint=sb://taskmonk.servicebus.windows.net/;SharedAccessKeyName=%s;SharedAccessKey=%s;EntityPath=%s", accessKeyName, accessKey, queueName);
    }

    public CompletableFuture<Void> send(String messageId, String label, String content) throws ServiceBusException, InterruptedException {
        QueueClient sendClient = new QueueClient(new ConnectionStringBuilder(connectionString, queueName), ReceiveMode.PEEKLOCK);
        Message message = new Message(content);
        message.setContentType("application/json");
        message.setLabel(label);
        message.setMessageId(messageId);
        message.setTimeToLive(Duration.ofDays(14));
        logger.debug("\nMessage sending: Id = {}", message.getMessageId());
        return sendClient.sendAsync(message);

    }
}
