package io.taskmonk.streaming.azure;

import com.microsoft.azure.servicebus.*;
import io.taskmonk.streaming.MessageAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;

public class AzureQueueMessageHandler implements IMessageHandler {
    private static final Logger logger = LoggerFactory.getLogger(AzureQueueMessageHandler.class);

    MessageHandler messageHandler;
    QueueClient queueClient;
    AzureQueueMessageHandler(QueueClient queueClient, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.queueClient = queueClient;
    }

    @Override
    public CompletableFuture<Void> onMessageAsync(IMessage message) {
        logger.trace("Processing {}", message.getLockToken());
        switch (message.getMessageBody().getBodyType()) {
            case BINARY:
                MessageAction result = null;
                try {
                    result = messageHandler.handle(
                            new String(message.getMessageBody().getBinaryData().get(0), StandardCharsets.UTF_8));
                    switch (result) {
                        case ABANDON:
                            logger.trace("Abandoning {}", message.getLockToken());
                            queueClient.abandon(message.getLockToken());
                            break;

                        case COMPLETE:
                            logger.trace("Completing {}", message.getLockToken());
                            queueClient.complete(message.getLockToken());
                            break;

                        default:
                            logger.trace("Invalid action {}", message.getLockToken());
                            queueClient.complete(message.getLockToken());
                            break;


                    }

                } catch (Exception ex) {
                    logger.error(message.getLockToken().toString(), ex);
                    try {
                        queueClient.deadLetter(message.getLockToken());
                    } catch (Exception e) {
                        logger.error(message.getLockToken().toString(), e);
                    }
                }

                break;
            default:
                logger.error("Unhandled message type : " + message.getMessageBody().getBodyType());
                try {
                    queueClient.deadLetter(message.getLockToken());
                } catch (Exception e) {
                    logger.error(message.getLockToken().toString(), e);
                }
        }
        return CompletableFuture.completedFuture(null);
    }

    @Override
    public void notifyException(Throwable exception, ExceptionPhase phase) {
        logger.error(phase.toString(), exception);

    }


}
