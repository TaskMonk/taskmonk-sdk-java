package io.taskmonk.streaming.azure;

import io.taskmonk.streaming.MessageAction;

import java.io.IOException;

public interface MessageHandler {
    public MessageAction handle(String message) throws IOException;
}
