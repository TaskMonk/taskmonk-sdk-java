package io.taskmonk.streaming.azure;
import com.microsoft.azure.servicebus.IMessageHandler;
import com.microsoft.azure.servicebus.MessageHandlerOptions;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.ReceiveMode;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;

import java.time.Duration;

public class ServiceBusListener {
    String queueName;
    String accessKey;

    public ServiceBusListener(String queueName, String accessKey) {
        this.queueName = queueName;
        this.accessKey = accessKey;
    }

    public Boolean addMessageHandler(MessageHandler messageHandler) throws ServiceBusException, InterruptedException {
        String accessKeyName = "Client";
        String connectionString = String.format("Endpoint=sb://taskmonk.servicebus.windows.net/;SharedAccessKeyName=%s;SharedAccessKey=%s;EntityPath=%s", accessKeyName, accessKey, queueName);

        QueueClient receiveClient = new QueueClient(new ConnectionStringBuilder(connectionString, queueName), ReceiveMode.PEEKLOCK);
        IMessageHandler azureMessageHandler = new AzureQueueMessageHandler(receiveClient, messageHandler);
        receiveClient.registerMessageHandler(azureMessageHandler, new MessageHandlerOptions(1, false, Duration.ofMinutes(1)));
        return true;

    }
}
