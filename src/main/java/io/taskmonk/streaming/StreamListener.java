package io.taskmonk.streaming;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import io.taskmonk.entities.BatchStatus;
import io.taskmonk.entities.Task;
import io.taskmonk.streaming.azure.MessageHandler;
import io.taskmonk.streaming.impl.MessageStreamListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Class to receive update messages from Taskmonk. The client should add a {@link MessageListener} to process each
 * of the messages. On completion of message processing, the handler should return a {@link MessageAction} which will
 * determine the action to be taken on the message. The semantics provide for Atleast Once processing and so the client
 * might receive duplicate messages.
 */
public class StreamListener {
    private static final Logger logger = LoggerFactory.getLogger(StreamListener.class);
       String queueName;
       String accessKey;
       MessageStreamListener messageStreamListener;

    /**
     * The queue name and access key are specific to a project and will be provided by Taskmonk
     * @param queueName
     * @param accessKey
     */
       public StreamListener(String queueName, String accessKey) {
           this.queueName = queueName;
           this.accessKey = accessKey;
           messageStreamListener = new MessageStreamListener(queueName, accessKey);
       }

    /**
     * Add a listener for messages from Taskmonk
     * @param listener {@link MessageListener} An implementation to handle messages received
     * @return true on succesfully adding the listener
     * @throws ServiceBusException
     * @throws InterruptedException
     */
       public Boolean addListener(MessageListener listener) throws ServiceBusException, InterruptedException {
           return messageStreamListener.addMessageHandler(new MessageHandler() {
               @Override
               public MessageAction handle(String message) throws IOException {
                   ObjectMapper mapper = new ObjectMapper();

                   JsonNode rootNode = mapper.readValue(message.getBytes(), JsonNode.class);
                   String messageType = rootNode.get("message_type").asText();
                   if (messageType.equalsIgnoreCase("task_update")) {
                       logger.trace("Handling task_update message");
                       JsonNode taskNode = rootNode.get("task");
                       Task task = mapper.treeToValue(taskNode, Task.class);
                       return listener.onTaskUpdate(task);
                   } else if (messageType.equalsIgnoreCase("batch_status")) {
                       logger.trace("Handling batch_status message");
                       JsonNode node = rootNode.get("batch");
                       BatchStatus batchStatus = mapper.treeToValue(node, BatchStatus.class);
                       return listener.onBatchStatus(batchStatus);
                   } else {
                       logger.error("Unrecognised message {}", messageType);
                       return listener.onGenericMessage(message);
                   }

               }
           });

       }
}

