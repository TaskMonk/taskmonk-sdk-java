package io.taskmonk.streaming;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.taskmonk.entities.Task;
import io.taskmonk.streaming.impl.MessageStreamWriter;
import io.taskmonk.streaming.impl.NewTasks;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Class to send messages upstream to TaskMonk
 */
public class StreamWriter {
    String queueName;
    String accessKey;
    MessageStreamWriter messageStreamWriter;
    public StreamWriter(String queueName, String accessKey) {
        this.queueName = queueName;
        this.accessKey = accessKey;
        messageStreamWriter = new MessageStreamWriter(queueName, accessKey);

    }

    /**
     * Send a list of new tasks to TaskMonk
     * @param projectId
     * @param batchId
     * @param tasks - Map of key value pairs for the tasks
     * @return A correlation id for the upload
     * @throws Exception
     */
    public String send(String projectId, String batchId, List<Task> tasks) throws Exception {
        NewTasks newTasks = new NewTasks();
        newTasks.project_id = projectId;
        newTasks.batch_id = batchId;
        newTasks.tasks = tasks;
        String messageId = UUID.randomUUID().toString();
        String label = String.format("%s:%s", projectId, batchId);
        String content = new ObjectMapper().writeValueAsString(newTasks);
        messageStreamWriter.send(messageId, label, content);
        return messageId;
    }
}
