package io.taskmonk.streaming;

import io.taskmonk.entities.BatchStatus;
import io.taskmonk.entities.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Interface for handling messages from Taskmonk
 */
public interface MessageListener {
    public static final Logger logger = LoggerFactory.getLogger(MessageListener.class);

    /**
     * Handle task update
     * @param task {@link Task}
     * @return {@link MessageAction} The action to take on handling message
     */
    public MessageAction onTaskUpdate(Task task);

    /**
     * Handle batchStatus
     * @param batchStatus {@link BatchStatus}
     * @return {@link MessageAction} The action to take on handling message
     */
    public MessageAction onBatchStatus(BatchStatus batchStatus);

    /**
     * Handle any generic message
     * @param message
     * @return {@link MessageAction} The action to take on handling message
     */
    public MessageAction onGenericMessage(String message);
}
