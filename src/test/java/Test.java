import io.taskmonk.auth.OAuthClientCredentials;
import io.taskmonk.client.TaskMonkClient;
import io.taskmonk.entities.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.Reader;
import java.util.*;

public class Test {
    private static final Logger logger = LoggerFactory.getLogger(Test.class);
    public static void main(String[] args) throws Exception {
        String batchId="1392";
        String projectId = "230";
        /**
         * Initialize the taskmonk client with the oauth credentials and projectId
         */
        TaskMonkClient client = new TaskMonkClient("preprod.taskmonk.io",
                new OAuthClientCredentials("67rgi7cZVg65A8KRs4o1tlsE3y1CvZ2p", "QMBotX8uavdiVZZImmetDZhOJyo0c6rfZ95FFIYJXfm8n03oZaoLtoy9ph9GjzJ0"));
        Map <String, Integer> result = new HashMap<String, Integer>();

        /*
         * Create batch with default parameters
         */

//        String batchId = client.createBatch(projectId, "tm_upload_test");
        Reader in = new FileReader("/tmp/records.tsv");
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().withDelimiter('\t').parse(in);

        /*
        List<Task> tasks = new ArrayList<Task>();
        int count = 0;
        for (CSVRecord record : records) {
            Map<String, String> data = record.toMap();
            tasks.add(new Task(batchId, data.get("item_id"), data));
            count++;
            if (count == 500) {
                System.out.println("Uploading");
                String jobId = client.uploadTasks(batchId, tasks);
                JobProgressResponse response = client.getJobProgress(jobId);
                while (!response.isCompleted()) {
                    System.out.println(response);
                    Thread.sleep(1000);
                    response = client.getJobProgress(jobId);
                }
                System.out.println("Finished upload");
                tasks = new ArrayList<Task>();
                count = 0;
            }
        }
        if (count > 0) {
            System.out.println("Uploading");
            String jobId = client.uploadTasks(batchId, tasks);
            JobProgressResponse response = client.getJobProgress(jobId);
            while (!response.isCompleted()) {
                System.out.println(response);
                Thread.sleep(1000);
                response = client.getJobProgress(jobId);
            }
            System.out.println("Finished upload");
            tasks = new ArrayList<Task>();
            count = 0;

        }


         */
        TaskQueryParams taskQueryParams = new TaskQueryParams();
        taskQueryParams.setPageSize(1);
        taskQueryParams.setTaskState("COMPLETED");
        taskQueryParams.setPageNumber(1000);
        taskQueryParams.setTaskCompleteStart(new Date());
        Page<Task> taskOutput = client.getBatchOutputDictionary(batchId, taskQueryParams);
        System.out.println("total tasks = " + taskOutput.getTotal());
        System.out.println("tasks returned = " + taskOutput.getPageSize());
        System.out.println("tasks returned = " + taskOutput.getItems());
        System.out.println(taskOutput.getItems().get(0));

        BatchStatus status = client.getBatchStatus(batchId);
        System.out.println("Batch status for " + batchId + " is " + status);
        /*
//        client.updateBatchState(batchId1, BatchState.CANCELLED);
//        System.out.println(client.getBatchStatus(batchId1));

//        String batchId1 = "917";
//        NewBatchData newBatchData = new NewBatchData("batch_name2")
//                                    .setStartTime(new Date())
//                                    .setPriority(10);
//        String batchId2 = client.createBatch(newBatchData);


        Map<String, String> task1Data = new HashMap<String, String>();
        task1Data.put("AA_Url", "value1");
        task1Data.put("Start_Time", "value2");
        Task task1 = new Task("external_id1", task1Data);

        Map<String, String> task2Data = new HashMap<String, String>();
        task2Data.put("AA_Url", "value2");
        task2Data.put("Start_Time", "value2");
        Task task2 = new Task("external_id2", task2Data);

        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task1);
        for (int i = 0; i<500000; i++) {
            tasks.add(task2);
        }
        client.uploadTasks(batchId, tasks);
        */


        /*
         * Check the batch status
         */
        /*
        BatchStatus batchStatus = client.getBatchStatus(batchId1);
        logger.info("Batch Status : {}", batchStatus);
        System.out.println("Batch Status : {}" + batchStatus.getEta());
        result.put("Completed", batchStatus.getCompleted());
        if (batchStatus.getCompleted() == batchStatus.getTotal()) {

        }
        result.put("In-progress", batchStatus.getCompleted());



        //Get the results for the batch with default params

        Page<Task> taskOutput = client.getBatchOutputDictionary(batchId);
        System.out.println("total tasks = " + taskOutput.getTotal());
        System.out.println("tasks returned = " + taskOutput.getPageSize());
        System.out.println("tasks returned = " + taskOutput.getItems());

        while (taskOutput.getItems().size() == taskOutput.getPageSize()) {
            // There are more tasks available
            TaskQueryParams taskQueryParams = new TaskQueryParams().setPageNumber(taskOutput.getPageNumber() + 1);
            taskOutput = client.getBatchOutputDictionary(batchId, taskQueryParams);
        }
        */

    }
}

