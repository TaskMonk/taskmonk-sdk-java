import io.taskmonk.auth.OAuthClientCredentials;
import io.taskmonk.client.TaskMonkClient;
import io.taskmonk.entities.TaskImportResponse;

import java.io.File;

public class UpdateBatchPriorityTest {
    public static void main(String[] args) throws Exception {
        String projectId = "609";
        System.out.println(System.getProperty("https.proxyHost"));
        /**
         * Initialize the taskmonk client with the oauth credentials and projectId
         */
        TaskMonkClient client = new TaskMonkClient("localhost:9000",
                new OAuthClientCredentials("uIUSPlDMnH8gLEIrnlkdIPRE6bZYhHpw", "zsYgKGLUnftFgkASD8pndMwn3viA0IPoGKAiw6S7aVukgMWI8hGJflFs0P2QYxTg"));
//        TaskMonkClient client = new TaskMonkClient(projectId, "https://preprod.taskmonk.io",
//                new OAuthClientCredentials("67rgi7cZVg65A8KRs4o1tlsE3y1CvZ2p", "QMBotX8uavdiVZZImmetDZhOJyo0c6rfZ95FFIYJXfm8n03oZaoLtoy9ph9GjzJ0"));

        TaskImportResponse uploadResponse = client.uploadTasks(projectId, "BatchPriorityTest", new File("/Users/dang/Downloads/Downloads2/Short_Primenow.xlsx"));
        System.out.println("Task Import Response = " +  uploadResponse);

        String batchId = client.updateBatchPriority(uploadResponse.batch_id, 1);
        System.out.println("Response from update batch priority = " +  batchId);
    }
}
